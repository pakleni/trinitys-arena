export interface Filter {
  display: string;
  filter: string;
}
