import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface CardSearchProps {
  handleFilterChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  filter: string;
}
const CardSearch: React.FC<CardSearchProps> = ({
  handleFilterChange,
  filter
}: CardSearchProps) => {
  return (
    <div className="field is-grouped">
      <div className="control is-expanded has-icons-left">
        <input
          className="input"
          type="text"
          placeholder="Find a card..."
          onChange={handleFilterChange}
          value={filter}
        />
        <span className="icon is-left">
          <FontAwesomeIcon icon="search" />
        </span>
      </div>
    </div>
  );
};

export default CardSearch;
