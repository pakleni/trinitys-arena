import React, { ReactElement, useState } from 'react';
import VizSensor from 'react-visibility-sensor';
import Link from 'next/link';
import Image from 'next/image';

interface CardElementProps {
  name?: string;
  floatyness?: number;
}

const CardElement = ({
  name,
  floatyness = 5
}: CardElementProps): ReactElement => {
  const [hover, setHover] = useState(false);
  const [visible, setVisible] = useState(false);

  const [coords, setCoords] = useState<string[]>(['0', '0']);

  const floatCard = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const node = e.target as HTMLElement;
    const rect = node.getBoundingClientRect();
    const x = e.clientX - rect.left;
    const y = e.clientY - rect.top;

    const newY = (x / rect.width - 1 / 2) * floatyness + 'deg';
    const newX = -(y / rect.height - 1 / 2) * floatyness + 'deg';

    setCoords([newX, newY]);
  };

  return (
    <VizSensor
      partialVisibility
      onChange={(isVisible) => {
        if (isVisible) setVisible(true);
      }}
    >
      <Link href={'/cards/[id]'} as={`/cards/${name}`}>
        <figure
          className="image"
          onMouseEnter={() => {
            setHover(true);
          }}
          onMouseLeave={() => {
            setHover(false);
          }}
          onMouseMove={(e) => floatCard(e)}
          style={{ zIndex: 2 }}
        >
          <Image
            alt="Card Image"
            id={name}
            width="500"
            height="700"
            quality={5}
            loading="eager"
            src={visible ? `/cards/${name}.png` : '/blank.png'}
            style={
              hover
                ? {
                    transform: `perspective(300px) rotateX(${coords[0]}) rotateY(${coords[1]})`
                  }
                : {
                    transform: 'perspective(300px) scaleZ(2) translateZ(-10px)'
                  }
            }
          />
        </figure>
      </Link>
    </VizSensor>
  );
};

export default CardElement;
