import React, { useState } from 'react';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import CardSearch from './CardSearch';
import { Card } from '../common/types';
import Image from 'next/image';
const links = [
  { href: '/guide', label: 'Guide' },
  { href: '/cards', label: 'Card Collection' }
].map((link) => ({
  ...link,
  key: `nav-link-${link.href}-${link.label}`
}));

interface NavProps {
  cards: Card[];
}

const Nav: React.FC<NavProps> = ({ cards }: NavProps) => {
  const [isShowing, setShowing] = useState(false);
  const [isSearch, setSearch] = useState(true);

  return (
    <>
      <div className="navbar-placeholder"></div>
      <nav className={`navbar is-fixed-top`}>
        <div className="navbar-brand">
          <Link
            href="/"
            className="navbar-item"
            onClick={() => setShowing(false)}
          >
            <Image
              alt="Trinity Logo"
              src="/trinity-logo.png"
              width="77"
              height="72"
              style={{
                maxHeight: '72px !important',
                width: 'auto'
              }}
            />
          </Link>
          <div className="navbar-item search-mobile is-hidden-desktop">
            <span
              onClick={() => setSearch(!isSearch)}
              role="button"
              className={`button search is-hidden-desktop ${
                !isSearch ? 'is-hidden is-fixed' : ''
              }`}
              style={
                cards.length === 0 ? { visibility: 'hidden', opacity: '0' } : {}
              }
            >
              <span className="icon">
                <FontAwesomeIcon icon="search" />
              </span>
            </span>
          </div>
          <div
            className={`navbar-item search search-mobile is-hidden-desktop ${
              isSearch ? ' is-fixed' : ''
            }`}
            style={
              isSearch
                ? { visibility: 'hidden', opacity: '0', pointerEvents: 'none' }
                : {}
            }
          >
            <CardSearch data={cards} width="150" />
          </div>

          <span
            onClick={() => setShowing(!isShowing)}
            role="button"
            className={`navbar-burger burger ${isShowing ? 'is-active' : ''}`}
            aria-label="menu"
            aria-expanded="false"
          >
            <span aria-hidden={true} />
            <span aria-hidden={true} />
            <span aria-hidden={true} />
          </span>
        </div>

        <div className={`navbar-menu ${isShowing ? 'is-active' : ''}`}>
          <div className="navbar-start">
            {links.map(({ key, href, label }) => (
              <Link
                key={key}
                href={href}
                className="navbar-item underline"
                onClick={() => setShowing(false)}
              >
                <b>{label}</b>
              </Link>
            ))}
          </div>
          <div className="navbar-end">
            <div className={'navbar-item'}>
              <div
                className="search is-hidden-touch"
                style={
                  cards.length === 0
                    ? { visibility: 'hidden', opacity: '0' }
                    : {}
                }
              >
                <CardSearch data={cards} width="200" />
              </div>
            </div>
          </div>
        </div>
        <style jsx>{`
          :global(body) {
            margin: 0;
            font-family:
              -apple-system,
              BlinkMacSystemFont,
              Avenir Next,
              Avenir,
              Helvetica,
              sans-serif;
          }
        `}</style>
      </nav>
    </>
  );
};

export default Nav;
