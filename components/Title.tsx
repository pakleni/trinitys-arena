import React from 'react';
import Head from 'next/head';

interface TitleProps {
  title?: string;
}

const Title: React.FC<TitleProps> = ({ title }: TitleProps) => {
  let chosenTitle = "Trinity's Arena - Living Card Game";

  if (title) chosenTitle = title + ' | ' + chosenTitle;

  return (
    <Head>
      <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
      <title>{chosenTitle}</title>
      <meta name="description" content={chosenTitle} />
    </Head>
  );
};
export default Title;
