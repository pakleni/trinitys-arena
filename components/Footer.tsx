import React, { ReactElement } from 'react';
import Link from 'next/link';
import Image from 'next/image';

const Footer: React.FC = (): ReactElement => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="columns is-multiline">
          <div className="column is-one-quarter-desktop">
            <ul>
              <li className="header">Links</li>
              <li>
                <Link href="/">Home</Link>
              </li>
              <li>
                <Link href="/guide">Guide</Link>
              </li>
              <li>
                <Link href="/cards">Card Collection</Link>
              </li>
            </ul>
          </div>

          <div className="column is-one-quarter-desktop">
            <ul>
              <li className="header">Contact us</li>
              <li>
                <a href="mailto: ognjenbjel@protonmail.com">
                  ognjenbjel@protonmail.com
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <br />
      <br className="is-hidden-desktop" />
      <a
        href="https://mandrake.games"
        target="_blank"
        rel="noopener noreferrer"
      >
        <div className="media center">
          <div className="media-left">
            <figure className="icon logo">
              <Image alt="Logo" src="/logo.png" width={75} height={75} />
            </figure>
          </div>
          <div className="media-content">
            <p className="text-center">Made by Mandrake Games</p>
          </div>
        </div>
      </a>
    </footer>
  );
};

export default Footer;
