export interface Card {
  ID: string;
  Name: string;
  Description: string;
  TYPE: string;
  CardArt: string;
}

export interface Deck {
  ID: number;
  Content: string;
  Name: string;
  Likes: number;
  MainCard: string;
}
