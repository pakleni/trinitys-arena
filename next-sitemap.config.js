module.exports = {
  siteUrl: 'https://trinity.mandrake.games/',
  generateRobotsTxt: true,
  sitemapSize: 7000,
  exclude: ['/server-sitemap.xml'], // <= exclude here
  robotsTxtOptions: {
    additionalSitemaps: [
      'https://trinity.mandrake.games/server-sitemap.xml' // <==== Add here
    ]
  }
};
